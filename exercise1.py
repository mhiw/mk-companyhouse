import numpy as np
import matplotlib.pyplot as plt

class arrayExercise:

    def __init__(self):
        self.array= np.zeros(1)

    def fillWithRandom(self, quanitity, randomList):
        for i in range(0, quanitity-1):
            self.array= np.append(self.array, self.array[i] + np.random.choice(randomList))

    def showPlot(self):
        plt.plot(self.array)
        plt.show()

    def max(self):
        return self.array.max()

    def min(self):
        return self.array.min()

    def overN(self, n):
        if self.max() > n:
            return True
        else:
            return False

    def printMaxMin(self):
        print('min= {}\nmax= {}'.format(self.min(), self.max()))

### __MAIN__ ###
difset= [-1, 1]
quanitity= 100
counter= 0
simulations= 50
overNval= 30

array= arrayExercise()
array.fillWithRandom(quanitity, difset)
array.printMaxMin()
array.showPlot()

for i in range(0, simulations):
    array= arrayExercise()
    array.fillWithRandom(quanitity, difset)
    if array.overN(overNval): counter+= 1 

testData= [simulations, counter]
testDataNames= ['under 30', 'over 30']
plt.pie(testData, labels=testDataNames, autopct='%1.1f%%')
plt.title('Table creation simulation steps')
plt.legend()
plt.show()

