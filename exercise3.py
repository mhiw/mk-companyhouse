import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import average_precision_score
from sklearn.linear_model import LinearRegression

wine= pd.read_csv('./winequality-red.csv')

qualityDivider= 4
groupsNames= [-1.0, 1.0] #-1 bad, 1 good
bins= (min(set(wine['quality']))-1, qualityDivider-0.5, max(wine['quality']))

y= pd.DataFrame(pd.cut(wine['quality'], bins = bins, labels = groupsNames))
x= wine.drop('quality', axis = 1)

xTrain, xTest, yTrain, yTest= train_test_split(x, y, test_size=0.33)

m= LinearRegression()
m= m.fit(xTrain, yTrain)
mPredicted= m.predict(xTest)
for i in range(0,len(mPredicted)):
    mPredicted[i]= 1 if mPredicted[i] >= 0 else -1


print("Accuracy= {:.4f}%".format(average_precision_score(yTest, mPredicted)*100))

