import pandas as pd

csvFile= './data.csv'

df= pd.read_csv(csvFile, sep=';', index_col='województwo', thousands=' ', decimal=',')
dfCols= df.columns
#a
print(df[df['powierzchnia [km²]'] > 20000])
#b
dfNew= df[df['ludność'] > 2000000]
#c
newRow= ['wielkopolskie', int('30'), 'Poznań', float('29 826,50'.replace(' ','').replace(',','.')), int('3 475 323'.replace(' ',''))]
df= df.append(pd.DataFrame([newRow[1:]], columns= dfCols, index= ['wielkopolskie']))
#d
df= df.sort_values('ludność', ascending=False)
#e
df= df.reindex([dfCols[0], dfCols[2], dfCols[3], dfCols[1]], axis=1)
#f
df= df.rename({i:i.capitalize() for i in df.index}, axis='index')
#g
data= {}
for index, row in df.iterrows():
    ppkm= row['ludność']/row['powierzchnia [km²]']
    if ppkm > 140: data[index]= ppkm 
        
s= pd.Series(data)
#h
df= df.drop(df[df['miasto wojewódzkie'] == 'Lublin'].index)
#i
print(df.describe())